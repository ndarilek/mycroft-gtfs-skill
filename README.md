# GTFS Skill

Ask [Mycroft](https://mycroft.ai) about data in [General Transit Feed Specification](http://gtfs.org/) format. Supported queries will include:

* "Tell me about stop 1068.": Provides a brief description of the stop and lists the next departure.
* "Which stop is on the south corner of Woodrow and Anderson?": Performs fuzzy string searches against stop descriptions. May not be accurate in all areas.
* "When is the next 323 westbound from stop 1068?": Searches static and real-time feeds for the next departure of the specified bus.
* "Alert me 10 minutes before the next 323 westbound departs stop 1068.": Trigger an alarm when the specified line is about to depart.
* "Refresh transit data.": Refreshes the locally cached GTFS static data. This also happens on a regular, daily schedule.
