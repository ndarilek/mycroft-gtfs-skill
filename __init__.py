# Copyright (c) 2017 Nolan Darilek
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime
from os.path import dirname, exists, join

from adapt.intent import IntentBuilder
from fuzzywuzzy import fuzz, process
# import geopandas
from mycroft.skills.core import MycroftSkill, intent_file_handler
from mycroft.util.log import getLogger
import pygtfs
import requests

__author__ = 'Nolan Darilek'

# Logger: used for debug lines, like "LOGGER.debug(xyz)". These
# statements will show up in the command line when running Mycroft.
LOGGER = getLogger(__name__)

class GtfsSkill(MycroftSkill):

    def __init__(self):
        super(GtfsSkill, self).__init__(name="GtfsSkill")
        self.gtfs_path = join(self.file_system.path, "gtfs.zip")

    @property
    def schedule(self):
        return pygtfs.Schedule(join(self.file_system.path, "gtfs.db"))

    def initialize(self):
        super(GtfsSkill, self).initialize()
        if not exists(self.gtfs_path) and self.settings.get("gtfsURL"):
            self.refresh_gtfs(speak_messages = True)

    def refresh_gtfs(self, speak_messages=False):
        if speak_messages:
            self.speak_dialog("refreshing")
            self.speak_dialog("wait")
        response = requests.get(self.settings.get("gtfsURL"), allow_redirects=True)
        gtfs = self.file_system.open("gtfs.zip", "w")
        gtfs.write(response.content)
        if speak_messages:
            self.speak_dialog("importing")
            self.speak_dialog("wait")
        gtfs.close()
        pygtfs.overwrite_feed(self.schedule, join(self.file_system.path, gtfs.name))
        if speak_messages:
            self.speak_dialog("done")

    @intent_file_handler("refresh.intent")
    def handle_refresh_intent(self, message):
        if self.settings.get("gtfsURL") is not None:
            self.refresh_gtfs(speak_messages = True)
        else:
            self.speak_dialog("no.transit.feed")

    def next_departure(self, stop):
        time = datetime.now().time()
        times = filter(lambda t: t.departure_time >= time, stop.stop_times)
        if len(times) > 0:
            t = times[0]
            return (t, t.trip.route)
        else:
            return (None, None)

    @intent_file_handler("stop.info.intent")
    def handle_stop_info_intent(self, message):
        stops = ["Stop "+stop.stop_id+": "+stop.stop_desc for stop in self.schedule.stops]
        stop = process.extractOne(message.data["stop"], stops, scorer=fuzz.partial_token_sort_ratio)
        if len(stops) == 0:
            self.speak_dialog("sorry")
            self.speak_dialog("no.match")
        else:
            self.speak(stop[0])

    def stop(self):
        pass

def create_skill():
    return GtfsSkill()
